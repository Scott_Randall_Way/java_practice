/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package javapractice;

/**
 *
 * @author SWay
 */
public class Palindrome {
    
    private String _palindrome;
    
    public Palindrome(String palindrome)
    {
        _palindrome = palindrome;     
    }
    
    public boolean isPalindrome()
    {
        int length = _palindrome.length();
        String reverse = new String();
        
        for (int i = length -1; i >= 0; i--)
        {
            reverse += _palindrome.charAt(i);
        }
        
        if (_palindrome.equals(reverse))
        {
            return true;            
        } 
        else
        {
            return false;
        }
        
    }
    
}

package javapractice;

/**
 * This is the main class designed for the JavaPractice project.
 * This project is designed to teach Java basics before learning
 * the FRC WPI libraries. 
 * 
 * @author Scott Way
 * @version 1.0
 * @since 09-02-2015
 */
public class Main {
	
	/**
	 * The program starts here.
	 * Java knows to start main first.
	 * 
	 * @param args - command line arguments
	 */
	public static void main(String[] args) {
		
		/* 
		 *   JAVA IS OBJECT ORIENTED
		 *   
		 *   Objects are represented with classes.
		 *   Below JavaPractic is a class.
		 *   There is a file for each class.
		 *   jp is an instance of that class.
		 *   You use the 'new' keyword to generate an instance of an object.
		 *   It is possible to have multiple instances of a class/object at the same time.
		 *   For example you could have student class and have 25 student instances.
		 *   You can think of a class as the definition of that object.      
		 */	
		
		/* 
		 *   SYNTAX NOTES
		 *   
		 *   Execution lines should end with a semicolon ';'.
		 *   Brackets '{' and '}' are used to mark the start and end of execution blocks, classes, and methods
		 *   Code inside brackets is indented to make it easier to read.  
		 */
				
		// create the practice object and run lessons   
		JavaPractice jp = new JavaPractice();		
		jp.AllLessons();
		
	}

}

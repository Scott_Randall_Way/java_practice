
package javapractice;

import java.util.*;

/**
 * This class contains various java lessons  
 * 
 * @author Scott Way
 * @version 1.0
 * @since 09-02-2015
 */
public class JavaPractice {

    /**
     * This method executes all the Java Practice Lessons
     */
    public final void AllLessons() {   
           	    	
    	// primitives
        primitives();
    	               
    	// lesson about classes
    	classExample();   	
    	
    	// if statements and loops
    	loopExample();
    	
//        // snake eyes test
//        System.out.println("\r\nHit enter to start trying for snake eyes:");
//        Scanner in = new Scanner(System.in);
//        in.nextLine();
//        SnakeEyes snakeEyes = new SnakeEyes();
//        int count = snakeEyes.getSnakeEyesCount();
//        System.out.println("\r\nIt took " + Integer.toString(count) + " tries.");
//               
//        
//        System.out.println("\r\nEnter integer to factorial:");
//        String fstr = in.nextLine();
//        
//        try
//        {
//            int fnum = Integer.parseInt(fstr);
//            Factorial factorial = new Factorial();
//            factorial.computeFactorial(fnum);
//        }
//        catch (Exception e)
//        {
//            System.out.println("You did not enter an integer fool.");
//        }        
    	
    }
    
    /** 
     *  This method provides an example of primitive data types
     */
    private void primitives()
    {
    	/* 
    	 *  PRIMITIVE VALUES
    	 *  
    	 *  Outside of standard class objects there variables can hold primitive data types.
    	 *  Here are some standard examples of java primitive data types.
    	 *  Java is a strongly typed language so you must use the correct types as parameters
    	 */
    	
    	/*
    	 *  SCOPE
    	 *  
    	 *  The scope of variables only extends to their code block ('{' to '}')  
    	 *  So for example none of the variables below would exist outside this method
    	 *  variables would be accessible inside nested code blocks for if statements and such.
    	 */   	
    	
    	int i = 100;  			// use for small whole numbers
    	long l = 218381;		// use for large whole numbers
    	double d = 143.832;		// use for decimal values
    	char c = 'x';			// use for single character
        boolean b = true;		// use for true or false  	    	
    }
    
    
    /**
     *  This method demonstrates a sample class
     */
    private void classExample()
    {
    	// make instances of student class 
    	Student bob = new Student("Bob Smith");
    	Student scott = new Student ("Scott Way"); 
    	
    	// print out bob
    	System.out.println("Student Name: " + bob.getStudentName());   	
    	System.out.println("Student Number: " + bob.getStudentIdentifier());
    	System.out.println();
    	
    	// print out scott
    	System.out.println("Student Name: " + scott.getStudentName());   	
    	System.out.println("Student Number: " + scott.getStudentIdentifier());
    	System.out.println();    	
    }   
    
    /**
     *  this method demonstrates various standard logic structures in Java
     */
    private void loopExample()
    {
    	// do example if statements
    	IfExample ie = new IfExample();
    	ie.doIfStatements();
    	
    	
    	
    }    
    
    private void palindromeTest()
    {    	
    	Scanner in = new Scanner(System.in);
        System.out.println("Enter string to test as palindrome:");
        Palindrome palindrome = new Palindrome(in.nextLine());
        if (palindrome.isPalindrome())
        {
            System.out.println("Yes that was a palindrome.");
        }
        else
        {
            System.out.println("No that was not a palindrome.");
        }     
        in.close();
    }   
    
}

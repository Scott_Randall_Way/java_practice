/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package javapractice;

/**
 *
 * @author Scott Way
 */
public class Factorial {
    
    public void computeFactorial(int fnum)
    {
        long factorial = 1;
        
        for (int i = 1; i <= fnum; i++)
        {
            factorial *= i;        
        }
        
        System.out.println("factorial of " + Integer.toString(fnum) +
                " is " + Long.toString(factorial));
        
        return;
    }
    
}

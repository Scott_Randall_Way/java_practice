/*
 *   CLASSES BELONG TO PACKAGES
 *   
 *   packages are just a way to organize and group classes.
 *   typically the packages mirror the folders, but not always
 *   classes in the same package can see each other.
 *      
 */
package javapractice;

/* 
 * 	USE IMPORT TO GET CLASSES FROM ANOTHER PACKAGE
 * 
 * 	We are going to use the Random class from the java.util package so we added an import.
 *  To import all the classes in a package use * like the java.util.* example below.    
 *  Try to only import the classes you need to keep your code base small rather than using *.
 *  Eclipse will show a warning if the imported package or class is not used like below.  
 */
import java.util.Random;    // import just random class
import java.util.*; 		// import all classes in java.util package

/** 
 * EXAMPLE CLASS
 * 
  There are basically three components of a class.
 * They are member variables, constructors, and methods.
 * Each of these is outlined below. 
 * Classes should be named in CamelCase.
 *  
 * @author Scott Way
 * @version 1.0
 * @since 09-01-2015
 */
public class Student 
{
	/*
	 * ACCESS MODIFIERS
	 * 
	 * Okay before talk about the parts of a class we need to explain 
	 * what access modifiers are.  In front of classes, methods, and 
	 * member variables you will see 'public' or 'private'.  
	 * There is also 'protected', but you won't see that as often.
	 * 'public' means it is accessible to all other objects
	 * 'private' means it is only accessible within an instance of this object.
	 * We'll cover 'protected' later when we learn about inheritance.     
	 */	
	
	/* 
	 * MEMBER VARIABLES
	 * 
	 * Member variables are properties of the object.
	 * In this example the student name and student number values are member variables.
	 * Member variables should be named in mixedCase style.
	 * Every instance will have these properties, but their values might be different.
	 * It is best practice to make member variables private.
	 * Member variables can be retrieved and modified through getter and setter methods.
	 * You will see examples of getters and setters below.
	 */
	
	private String studentName;
	private int studentNumber;
	
	/*
	 *  CONSTANTS
	 *  
	 *  Constants are variables that don't change.  
	 *  A good example would be days in a week.   
	 *  Constants should be named in all upper case letters.
	 *  Here we use the 'final' modifier to tell the compiler these values won't change.
	 */	
	private final int MINIMUM_STUDENT_NUMBER = 1000000;
	private final int MAXIMUM_STUDENT_NUMBER = 9999999;
	
	/*
	 *  CONSTRUCTORS
	 *  
	 *  The constructor will return an instance of the class.
	 *  Sometimes there things you need to do to initialize an instance.
	 *  These actions are captured in the constructor.
	 *  The method for the constructor will match the class name.
	 *  You can have multiple constructors for a class, but they must have different signatures.
	 *  Below we have a constructor that takes no arguments, and one that takes the student name.
	 *  If no initialization is needed you don't need to provide a constructor.  
	 */	
	
	// default constructor
	public Student()
	{		
        studentNumber = MakeRandomNumber();			 
	}	
	
	/*
	 *  PARAMETERS
	 *  
	 *  Methods and constructors can accept parameters.
	 *  Parameters appear inside the parenthesis after the method or constructor name.
	 *  Parameters are separated by commas.
	 *  You must identify the class or primitive data type of the parameter.
	 *  You can then use the parameter inside the method by the parameter name.
	 */
	
	// this constructor takes student's name
	public Student(String studentName)
	{
		studentNumber = MakeRandomNumber();	
		setStudentName(studentName);
	}
	
	/*
	 *  METHODS
	 *  
	 *  Methods are functions that allow you to interact with the object.
	 *  Setter and Getter methods are used to modify member variables.
	 *  Member variables are typically private to allow for the methods to contain rules.
	 *  For example maybe you want to insure a member variable for sex can only be set to 'M' or 'F'.
	 *  You would have a private member variable to hold the value and the logic to check the value in the setter.
	 *  Method Names should be in mixedCase.
	 */
	
	/*	 
	 *	RETURN VALUES	
	 *  
	 *  After the access modifier you will notice a data type or the word 'void'.  
	 *  This is the class or primitive data type that will be returned.  
	 *  When 'void' is used that means nothing is returned.
	 *    
	 */	
	public void setStudentName(String studentName)
	{
		this.studentName = studentName; 
	}
	
	public String getStudentName()
	{
		return studentName;
	}
	
	/* 
	 *  'THIS' IS A KEYWORD
	 *  
	 *  You can use this to refer to the current object.
	 *  This is the typical way to refer to member variables. 
	 */
	
	public void setStudentNumber(int studentNumber)
	{
		this.studentNumber = studentNumber;
	}	
	
	public int getStudentNumber()
	{
		return studentNumber;
	}
	
	// converts student number to string
	public String getStudentIdentifier()
	{
		return Integer.toString(this.studentNumber);
	}
	
	// this is a private method so it cannot be accessed outside of an instance 
	private int MakeRandomNumber()
	{
		Random random = new Random();
		return random.nextInt((MAXIMUM_STUDENT_NUMBER - MINIMUM_STUDENT_NUMBER) + 1) + MINIMUM_STUDENT_NUMBER;
	}
	
}

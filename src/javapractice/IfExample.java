package javapractice;

/**
 * This class shows off different if statement examples
 * @author Scott Way
 * @version 1.0
 * @since 09-01-2015
 */
public class IfExample {

	public void doIfStatements()
	{
		/* 
    	 *  IF STATEMENTS
    	 *  
    	 *  If statements must resolve to a true or false.
    	 *  Use ==, !=, >=, <= to evaluate values
    	 *  Use && and || to compound statements together.
    	 *  && stands for an 'and' operation
    	 *  || stands for an 'or' operation
    	 *  use an || operation to do a short circuit
    	 */
    	
    	boolean b = true;
    	boolean c = true;
    	
    	// standard if statement
    	if (b == true)
    	{
    		System.out.println("b is true");
    	}
    	
    	// standard if else statement
    	if (b == false)
    	{
    		System.out.println("b is false");
    	}
    	else
    	{
    		System.out.println("b is still true");
    	}
    	
    	// standard if else if statement
    	if (b == false || c == false)
    	{
    		System.out.println("b is still false");
    	}
    	else if (c == false)		
    	{
    		System.out.println("c is also false;");
    	}
    	else if (c == true && b == true)
    	{
    		System.out.println("c is true");
    	}
	}
	
}

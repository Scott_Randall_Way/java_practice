/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package javapractice;

import java.util.Random;

/**
 *
 * @author SWay
 */
public class SnakeEyes {
    
    private static int MINIMUM = 2;
    private static int MAXIMUM = 12;
    
    public int getSnakeEyesCount()
    {
        int rollCount = 0;
        
        
        boolean snakeEyes = false;
        while (!snakeEyes)
        {
            rollCount++;
            Random random = new Random();
            int dice = random.nextInt((MAXIMUM - MINIMUM) + 1) + MINIMUM;
            System.out.println("rolled: " + Integer.toString(dice));
            if (dice == MINIMUM)
            {
                snakeEyes = true;
            }            
        }
        
        return rollCount;        
    }    
    
}
